package com.freenow.assignment

import com.freenow.assignment.enums.FleetType
import com.freenow.assignment.extensions.toUiModel
import com.freenow.assignment.network.io.model.VehiclesResponse
import com.google.gson.Gson
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class VehicleExtensionsTest {

    private lateinit var vehiclesResponse: VehiclesResponse

    private fun getVehicleResponseObject(): VehiclesResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource("vehicles_response.json").toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, VehiclesResponse::class.java)
    }

    @Before
    fun init() {
        vehiclesResponse = getVehicleResponseObject()
    }

    @Test
    fun `the SELECTED value in UIVehicleItem should be false when converted from vehicle object`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        Assert.assertFalse(uiModel.selected)
    }

    @Test
    fun `the default overlay color should be valid when vehicle converted to UIVehicleItem`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        Assert.assertTrue(
            uiModel.selectedOverlayColor == R.color.greylight &&
                uiModel.unSelectedOverlayColor == R.color.transparent
        )
    }

    @Test
    fun `the the taxi icon should be Pooling if fleet type is POOLING`() {
        val vehicle = vehiclesResponse.vehicles!![2]
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        Assert.assertTrue(uiModel.fleetType == FleetType.POOLING)
    }

    @Test
    fun `the the taxi icon should be TAXI if fleet type is TAXI`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        Assert.assertTrue(uiModel.fleetType == FleetType.TAXI)
    }

    @Test
    fun `the the taxi icon and the fleet type should be relevant`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        val expectedDrawableId = if (vehicle.fleetType == FleetType.TAXI) {
            R.drawable.ic_taxi
        } else {
            R.drawable.ic_pool
        }
        Assert.assertTrue(uiModel.icon == expectedDrawableId)
    }

    @Test
    fun `heading value should never be null`() {
        val vehicleWithNullHeading = vehiclesResponse.vehicles!!.first().copy(heading = null)
        val anyPosition = 0
        val uiModel = vehicleWithNullHeading.toUiModel(anyPosition)
        Assert.assertTrue(uiModel.heading == "0")
    }

    @Test
    fun `vehicle coordinates should never be null`() {
        val vehicleWithNullCoordinates = vehiclesResponse.vehicles!!.first().copy(coordinate = null)
        val anyPosition = 0
        val uiModel = vehicleWithNullCoordinates.toUiModel(anyPosition)
        Assert.assertTrue(uiModel.latitude == "0" && uiModel.longitude == "0")
    }

    @Test
    fun `vehicle coordinates and UIVehicleItem coordinates should match`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        val status = uiModel.latitude == vehicle.coordinate!!.latitude.toString() &&
            uiModel.longitude == vehicle.coordinate!!.longitude.toString()
        Assert.assertTrue(status)
    }

    @Test
    fun `the selected and unselected overlay colors should be valid`() {
        val vehicle = vehiclesResponse.vehicles!!.first()
        val anyPosition = 0
        val uiModel = vehicle.toUiModel(anyPosition)
        Assert.assertTrue(
            uiModel.selectedOverlayColor == R.color.greylight &&
                uiModel.unSelectedOverlayColor == R.color.transparent
        )
    }
}
