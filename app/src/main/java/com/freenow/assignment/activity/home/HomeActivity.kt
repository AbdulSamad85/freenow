package com.freenow.assignment.activity.home

import android.os.Bundle
import com.freenow.assignment.abstraction.BaseActivity
import com.freenow.assignment.databinding.ActivityHomeBinding
import com.freenow.assignment.fragment.map.MapFragment
import com.freenow.assignment.fragment.vehiclesList.VehiclesListFragment
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : BaseActivity() {
    private lateinit var binding: ActivityHomeBinding
    private var fromLocation: LatLng = LatLng(53.694865, 9.757589)
    private var toLocation: LatLng = LatLng(53.394655, 10.099891)

    @Inject
    lateinit var homeFragmentFactory: HomeFragmentFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loadMapFragment(fromLocation, toLocation)
        loadVehicleListFragment()
    }

    private fun loadMapFragment(fromLocation: LatLng, toLocation: LatLng) {
        supportFragmentManager.fragmentFactory = homeFragmentFactory
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(
                binding.mapContainer.id, MapFragment::class.java,
                MapFragment.createBundle(fromLocation, toLocation)
            ).commit()
    }

    private fun loadVehicleListFragment() {
        supportFragmentManager.fragmentFactory = homeFragmentFactory
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(
                binding.vehiclesListContainer.id, VehiclesListFragment::class.java, null
            ).commit()
    }
}
