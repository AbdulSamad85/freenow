package com.freenow.assignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.freenow.assignment.viewmodel.HomeSharedViewModel
import com.freenow.assignment.enums.FleetType
import com.freenow.assignment.extensions.toUiModel
import com.freenow.assignment.network.io.model.Coordinate
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.network.io.model.VehiclesResponse
import com.freenow.assignment.repository.VehicleRepository
import com.google.android.gms.maps.model.LatLng
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class HomeSharedViewModelTest {
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var vehicleRepository: VehicleRepository
    private lateinit var viewModel: HomeSharedViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        viewModel = HomeSharedViewModel(vehicleRepository)
    }

    @Test
    fun `when loadVehicles() call, should load vehicles data`() {
        val anyDouble = 123.0
        val anyInt = 0
        val fromLatLng = LatLng(anyDouble, anyDouble)
        val toLatLng = LatLng(anyDouble, anyDouble)
        val vehicle = Vehicle(
            id = anyInt,
            fleetType = FleetType.TAXI,
            heading = anyDouble,
            coordinate = Coordinate(anyDouble, anyDouble)
        )
        val vehiclesResponse = VehiclesResponse(listOf(vehicle))
        val expectedUiModel = listOf(vehicle.toUiModel(anyInt))
        runBlocking {
            Mockito.`when`(vehicleRepository.getVehicles(fromLatLng, toLatLng))
                .thenReturn(Response.success(vehiclesResponse))
            viewModel.getVehicles(fromLatLng, toLatLng)
            val result = viewModel.vehiclesLiveData.getOrAwaitValue()
            assertEquals(expectedUiModel, result)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}
