package com.freenow.assignment.activity.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.freenow.assignment.fragment.map.MapFragment
import com.freenow.assignment.fragment.vehiclesList.VehiclesListFragment
import javax.inject.Inject

class HomeFragmentFactory
@Inject
constructor() : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        return when (className) {
            MapFragment::class.java.name -> {
                MapFragment()
            }
            VehiclesListFragment::class.java.name -> {
                VehiclesListFragment()
            }
            else -> return super.instantiate(classLoader, className)
        }
    }
}
