package com.freenow.assignment.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.freenow.assignment.R
import com.freenow.assignment.databinding.ItemVehicleBinding
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.ui_model.UIVehicleItem
import com.squareup.picasso.Picasso

/**
 * Custom view class to show the vehicle list item
 */
class VehicleListItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
) : ConstraintLayout(context, attrs) {

    interface SelectableVehicleCallbacks {
        fun onVehicleItemClicked(position: Int, vehicle: Vehicle)
    }

    private var callbacks: SelectableVehicleCallbacks? = null
    fun setListener(selectableVehicleCallbacks: SelectableVehicleCallbacks?) {
        callbacks = selectableVehicleCallbacks
    }

    private lateinit var uiVehicle: UIVehicleItem
    private val binding = ItemVehicleBinding.inflate(LayoutInflater.from(context), this, true)

    private val imageHeight = context.resources.getDimensionPixelSize(R.dimen.vehicle_image_height)
    private val imageWidth = context.resources.getDimensionPixelSize(R.dimen.vehicle_image_width)

    fun updateUi(uiVehicle: UIVehicleItem) {
        this.uiVehicle = uiVehicle
        setListeners()
        setImage(uiVehicle.icon)
        setTitle(uiVehicle.fleetType.value)
        setHeading(uiVehicle.heading)
        setCoordinates(uiVehicle.latitude, uiVehicle.longitude)
        setOverylayColor(getOverlayColor(uiVehicle.selected))
    }

    private fun getOverlayColor(isSelected: Boolean): Int {
        return if (isSelected) {
            uiVehicle.selectedOverlayColor
        } else {
            uiVehicle.unSelectedOverlayColor
        }
    }

    private fun setListeners() {
        binding.overlay.setOnClickListener {
            callbacks?.onVehicleItemClicked(uiVehicle.position, uiVehicle.vehicle)
        }
    }

    private fun setTitle(title: String) {
        binding.title.text = title
    }

    private fun setHeading(headingTowards: String) {
        binding.heading.text = headingTowards
    }

    private fun setCoordinates(latitude: String, longitude: String) {
        binding.coordinates.text = resources.getString(R.string.coordinates, latitude, longitude)
    }

    private fun setImage(resourceId: Int) {
        loadImage(resourceId, binding.imgVehicle)
    }

    private fun loadImage(resourceId: Int, imageView: ImageView) {
        Picasso
            .with(context)
            .load(resourceId)
            .resize(imageWidth, imageHeight)
            .error(R.drawable.ic_error)
            .into(imageView)
    }

    private fun setOverylayColor(colorResourceId: Int) {
        binding.overlay.setImageResource(colorResourceId)
    }
}
