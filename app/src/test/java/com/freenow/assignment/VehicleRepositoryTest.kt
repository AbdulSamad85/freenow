package com.freenow.assignment

import com.freenow.assignment.network.VehicleService
import com.freenow.assignment.network.io.model.VehiclesResponse
import com.freenow.assignment.repository.VehicleRepository
import com.freenow.assignment.repository.VehicleRepositoryImpl
import com.google.android.gms.maps.model.LatLng
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnit4::class)
class VehicleRepositoryTest {

    lateinit var vehicleRepository: VehicleRepository

    @Mock
    lateinit var vehicleService: VehicleService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        vehicleRepository = VehicleRepositoryImpl(vehicleService)
    }

    @Test
    fun `get all vehicles test`() {
        val anyDouble = 123.0
        val fromLatLng = LatLng(anyDouble, anyDouble)
        val toLatLng = LatLng(anyDouble, anyDouble)

        val expectedResponse = VehiclesResponse(emptyList())
        runBlocking {
            Mockito.`when`(vehicleRepository.getVehicles(fromLatLng, toLatLng))
                .thenReturn(
                    Response.success(
                        VehiclesResponse(emptyList())
                    )
                )
            val response = vehicleRepository.getVehicles(fromLatLng, toLatLng)
            assertEquals(expectedResponse, response.body())
        }
    }
}
