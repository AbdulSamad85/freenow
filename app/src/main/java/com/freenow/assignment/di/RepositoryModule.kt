package com.freenow.assignment.di

import com.freenow.assignment.network.VehicleService
import com.freenow.assignment.repository.VehicleRepository
import com.freenow.assignment.repository.VehicleRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {
    @Singleton
    @Provides
    fun provideVehicleRepository(vehicleService: VehicleService): VehicleRepository {
        return VehicleRepositoryImpl(vehicleService)
    }
}
