package com.freenow.assignment.fragment.map

import android.os.Bundle
import android.view.View
import androidx.core.view.doOnLayout
import androidx.fragment.app.activityViewModels
import com.freenow.assignment.R
import com.freenow.assignment.abstraction.BaseFragment
import com.freenow.assignment.viewmodel.HomeSharedViewModel
import com.freenow.assignment.common.Utils
import com.freenow.assignment.databinding.FragmentMapBinding
import com.freenow.assignment.extensions.observeNonNull
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.ui_model.UIVehicleItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : BaseFragment(R.layout.fragment_map), OnMapReadyCallback {

    private val viewModel: HomeSharedViewModel by activityViewModels()
    private lateinit var binding: FragmentMapBinding
    private var map: GoogleMap? = null
    private lateinit var defaultMapBounds: LatLngBounds // for showing map bounded to required location
    private var cameraChangeReason = REASON_GESTURE // determines if map is moved by user

    companion object {
        private const val MARKER_PADDING = 60
        private const val KEY_MAP_BOUNDS = "KEY_FROM_LOCATION"

        /**
         * Utility method to create bundle for fragment
         */
        fun createBundle(vararg latLng: LatLng): Bundle {
            return Bundle().apply {
                val latLngBoundsBuilder = LatLngBounds.Builder()
                latLng.map { latLngBoundsBuilder.include(it) }
                putParcelable(KEY_MAP_BOUNDS, latLngBoundsBuilder.build())
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMapBinding.bind(view)
        arguments?.let { handleBundle(it) }
        observeData()
        loadMap()
    }

    private fun handleBundle(bundle: Bundle) {
        defaultMapBounds = bundle.getParcelable(KEY_MAP_BOUNDS)!!
    }

    /**
     * Draw all the marks on the map, then animate camera to show all markers
     */
    private fun drawMarkers(list: List<UIVehicleItem>) {
        // TODO use clustering
        val latLngBoundsBuilder = LatLngBounds.Builder()
        list.map {
            val markerPosition = LatLng(
                it.latitude.toDouble(),
                it.longitude.toDouble()
            )
            latLngBoundsBuilder.include(markerPosition)
            map?.addMarker(MarkerOptions().position(markerPosition))
        }
        if (list.isNotEmpty()) {
            animateCamera(latLngBoundsBuilder.build())
        }
    }

    private fun observeData() {
        viewModel.vehiclesLiveData.observeNonNull(this) {
            drawMarkers(it)
        }

        viewModel.selectedVehicle.observe(viewLifecycleOwner) { vehicle: Vehicle? ->
            if (vehicle != null) {
                vehicle.coordinate?.let {
                    animateCamera(LatLng(it.latitude, it.longitude))
                }
            } else {
                animateCamera(defaultMapBounds)
            }
        }
    }

    private fun loadMap() {
        val mapFragment: SupportMapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        setupMap(googleMap)
        setMapListeners(googleMap)
        loadVehicles(defaultMapBounds.northeast, defaultMapBounds.southwest)
    }

    private fun loadVehicles(fromLocation: LatLng, toLocation: LatLng) {
        viewModel.getVehicles(fromLocation, toLocation)
    }

    /**
     * Sets padding to map, and moves the camera to initial location to
     * avoid the showing of world map
     */
    private fun setupMap(map: GoogleMap) {
        map.setPadding(MARKER_PADDING / 2, MARKER_PADDING, MARKER_PADDING / 2, 0)
        binding.root.doOnLayout {
            // moving camera to initial position so world map may not show
            map.moveCamera(
                CameraUpdateFactory.newLatLngBounds(
                    defaultMapBounds, it.measuredWidth,
                    it.measuredHeight, MARKER_PADDING
                )
            )
        }
    }

    /**
     * detects if user moved the map himself(not through code),  new data
     * is loaded soon as map movement stops
     */
    private fun setMapListeners(map: GoogleMap) {
        map.setOnCameraMoveStartedListener { reason ->
            cameraChangeReason = reason
        }
        // loading new data when map stops moving
        map.setOnCameraIdleListener {
            if (cameraChangeReason == REASON_GESTURE) {
                // user performed interaction
                removeMarkers()
                val defaultMapBounds = map.projection.visibleRegion.latLngBounds
                loadVehicles(defaultMapBounds.northeast, defaultMapBounds.southwest)
            }
        }
    }

    /**
     * Removes all markers on map
     */
    private fun removeMarkers() {
        map?.clear()
    }

    private fun animateCamera(latLng: LatLng) {
        map?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLngBounds: LatLngBounds) {
        val adjustedBounds = Utils.getMaxZoomedBounds(latLngBounds)
        map?.animateCamera(CameraUpdateFactory.newLatLngBounds(adjustedBounds, MARKER_PADDING))
    }
}
