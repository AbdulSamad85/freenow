package com.freenow.assignment.ui_model

import android.os.Parcelable
import com.freenow.assignment.R
import com.freenow.assignment.enums.FleetType
import com.freenow.assignment.network.io.model.Vehicle
import kotlinx.parcelize.Parcelize

/**
 * UIModel for custom Vehicle view
 */
@Parcelize
data class UIVehicleItem(
    val vehicle: Vehicle,
    val position: Int,
    val icon: Int,
    val fleetType: FleetType,
    val heading: String,
    val latitude: String,
    val longitude: String,
    var selected: Boolean = false,
    var selectedOverlayColor: Int = R.color.greylight,
    var unSelectedOverlayColor: Int = R.color.transparent,
) : Parcelable