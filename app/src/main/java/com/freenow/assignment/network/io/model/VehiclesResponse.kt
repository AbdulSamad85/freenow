package com.freenow.assignment.network.io.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class VehiclesResponse(
    @SerializedName("poiList")
    val vehicles: List<Vehicle>?
) : Parcelable
