package com.freenow.assignment.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.freenow.assignment.extensions.toUiModel
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.repository.VehicleRepository
import com.freenow.assignment.ui_model.UIVehicleItem
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeSharedViewModel
@Inject constructor(
    private val vehicleRepository: VehicleRepository
) : ViewModel() {
    val selectedVehicle: LiveData<Vehicle?> get() = _selectedVehicle
    private val _selectedVehicle: MutableLiveData<Vehicle?> = MutableLiveData()

    val vehiclesLiveData: LiveData<List<UIVehicleItem>> get() = _vehiclesLiveData
    private val _vehiclesLiveData: MutableLiveData<List<UIVehicleItem>> = MutableLiveData()

    val viewActionsLiveData: LiveData<Boolean> get() = _viewActionsLiveData
    private val _viewActionsLiveData: MutableLiveData<Boolean> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError("Exception: : ${throwable.localizedMessage}")
    }

    fun getVehicles(fromLocation: LatLng, toLocation: LatLng) {

        viewModelScope.launch(exceptionHandler) {
            showProgress(true)
            val response = vehicleRepository.getVehicles(
                fromLocation,
                toLocation
            )
            if (response.isSuccessful) {
                showProgress(false)
                response.body()?.vehicles?.let {
                    val uiVehicleList = mutableListOf<UIVehicleItem>()
                    for (i in it.indices) {
                        uiVehicleList.add(it[i].toUiModel(i))
                    }
                    _vehiclesLiveData.postValue(uiVehicleList)
                }
            } else {
                onError("Error : ${response.message()} ")
            }
        }
    }

    private fun showProgress(show: Boolean) {
        _viewActionsLiveData.postValue(show)
    }

    fun setSelectedVehicle(vehicle: Vehicle?) {
        _selectedVehicle.value = vehicle
    }

    private fun onError(message: String) {
        showProgress(false)
    }
}
