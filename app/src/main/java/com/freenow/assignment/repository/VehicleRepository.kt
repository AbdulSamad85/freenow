package com.freenow.assignment.repository

import com.freenow.assignment.network.io.model.VehiclesResponse
import com.google.android.gms.maps.model.LatLng
import retrofit2.Response

interface VehicleRepository {
    suspend fun getVehicles(fromLocation: LatLng, toLocation: LatLng): Response<VehiclesResponse>
}
