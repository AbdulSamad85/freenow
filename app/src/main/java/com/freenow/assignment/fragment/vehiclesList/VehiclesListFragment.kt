package com.freenow.assignment.fragment.vehiclesList

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.freenow.assignment.R
import com.freenow.assignment.abstraction.BaseFragment
import com.freenow.assignment.adapter.VehiclesListAdapter
import com.freenow.assignment.databinding.FragmentVehiclesListBinding
import com.freenow.assignment.extensions.observeNonNull
import com.freenow.assignment.listener.VehiclesListCallbacks
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.viewmodel.HomeSharedViewModel

class VehiclesListFragment : BaseFragment(R.layout.fragment_vehicles_list), VehiclesListCallbacks {
    private lateinit var vehiclesListAdapter: VehiclesListAdapter
    private lateinit var binding: FragmentVehiclesListBinding
    private val viewModel: HomeSharedViewModel by activityViewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentVehiclesListBinding.bind(view)
        initRecyclerView()
        observeData()
        binding.swipeRefreshLayout.isEnabled = false
    }

    private fun initRecyclerView() {
        vehiclesListAdapter = VehiclesListAdapter(this, requireContext(), emptyList())
        binding.vehiclesRecyclerView.layoutManager = LinearLayoutManager(context).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        binding.vehiclesRecyclerView.adapter = vehiclesListAdapter
    }

    private fun observeData() {
        observeVehiclesLiveData()
    }

    private fun observeVehiclesLiveData() {
        viewModel.vehiclesLiveData.observeNonNull(this) {
            vehiclesListAdapter.setData(it)
            vehiclesListAdapter.notifyDataSetChanged()
        }

        viewModel.viewActionsLiveData.observeNonNull(this) {
            showProgressBar(it)
        }
    }

    override fun onVehicleSelected(position: Int, vehicle: Vehicle) {
        viewModel.setSelectedVehicle(vehicle)
    }

    override fun onNothingSelected() {
        viewModel.setSelectedVehicle(null)
    }

    private fun showProgressBar(show: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = show
    }
}
