package com.freenow.assignment

import com.freenow.assignment.network.VehicleService
import com.freenow.assignment.network.io.model.VehiclesResponse
import com.google.gson.Gson
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

@RunWith(JUnit4::class)
class VehicleServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var vehicleService: VehicleService

    private fun getVehicleResponseObject(): VehiclesResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource("vehicles_response.json").toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, VehiclesResponse::class.java)
    }

    private fun getVehiclesJson(): String {
        val jsonStream: InputStream =
            VehiclesResponse::class.java.classLoader!!.getResourceAsStream("vehicles_response.json")
        return String(jsonStream.readBytes())
    }

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        mockWebServer = MockWebServer()
        vehicleService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build().create(VehicleService::class.java)
    }

    @Test
    fun `get all movie api test`() {

        val anyDouble = 123.0

        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody(getVehiclesJson()))
            val response = vehicleService.getVehicles(anyDouble, anyDouble, anyDouble, anyDouble)
            val request = mockWebServer.takeRequest()
            val expectedPath =
                "/?p1Lat=$anyDouble&p1Lon=$anyDouble&p2Lat=$anyDouble&p2Lon=$anyDouble"
            assertEquals(expectedPath, request.path)
            assertEquals(true, response.body() == getVehicleResponseObject())
        }
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}
