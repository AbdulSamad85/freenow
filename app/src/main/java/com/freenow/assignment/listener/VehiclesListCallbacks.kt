package com.freenow.assignment.listener

import com.freenow.assignment.network.io.model.Vehicle

/**
 * Notifies the container when user selects or unselect a vehicle item
 */
interface VehiclesListCallbacks {
    fun onVehicleSelected(position: Int, vehicle: Vehicle)
    fun onNothingSelected()
}
