package com.freenow.assignment.extensions

import com.freenow.assignment.R
import com.freenow.assignment.common.Utils
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.ui_model.UIVehicleItem

/**
 * converts Vehicles to UIVehicleItem
 */
fun Vehicle.toUiModel(position: Int): UIVehicleItem {
    return UIVehicleItem(
        vehicle = this,
        position = position,
        icon = Utils.getIconResourceId(this.fleetType),
        fleetType = this.fleetType,
        heading = (this.heading ?: 0).toString(),
        latitude = (this.coordinate?.latitude ?: 0).toString(),
        longitude = (this.coordinate?.longitude ?: 0).toString(),
        selected = false,
        selectedOverlayColor = R.color.greylight,
        unSelectedOverlayColor = R.color.transparent
    )
}
