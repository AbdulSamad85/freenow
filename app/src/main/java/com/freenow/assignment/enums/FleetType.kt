package com.freenow.assignment.enums

enum class FleetType(val value: String) {
    TAXI("TAXI"),
    POOLING("POOLING"); // car pooling
}
