package com.freenow.assignment.network.io.model

import android.os.Parcelable
import com.freenow.assignment.enums.FleetType
import kotlinx.parcelize.Parcelize

@Parcelize
data class Vehicle(
    val id: Int,
    val fleetType: FleetType,
    val heading: Double?,
    val coordinate: Coordinate?
) : Parcelable
