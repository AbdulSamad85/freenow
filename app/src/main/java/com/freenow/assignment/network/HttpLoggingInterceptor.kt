package com.freenow.assignment.network

import android.util.Log
import com.freenow.assignment.BuildConfig
import okhttp3.*
import okio.Buffer
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import javax.inject.Inject

/**
 * To provide detailed logging of API calls
 */
class HttpLoggingInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        Log.d(BuildConfig.APPLICATION_ID, "call ==> " + request.url)
        val response: Response = chain.proceed(request)
        val responseBody: ResponseBody? = response.body
        val source = responseBody?.source()
        source?.request(Long.MAX_VALUE) // Buffer the entire body.
        val buffer: Buffer? = source?.buffer
        Log.d(
            BuildConfig.APPLICATION_ID,
            "response ==> " + buffer?.clone()
                ?.readString(Charset.forName(StandardCharsets.UTF_8.name())).toString()
        )
        return response
    }
}
