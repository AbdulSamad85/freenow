package com.freenow.assignment.repository

import com.freenow.assignment.network.VehicleService
import com.google.android.gms.maps.model.LatLng

class VehicleRepositoryImpl
constructor(private val vehicleService: VehicleService) : VehicleRepository {

    override suspend fun getVehicles(fromLocation: LatLng, toLocation: LatLng) =
        vehicleService.getVehicles(
            fromLocation.latitude, fromLocation.longitude,
            toLocation.latitude, toLocation.longitude
        )
}
