package com.freenow.assignment.adapter

import android.content.Context
import android.view.ViewGroup
import com.freenow.assignment.abstraction.BaseRecyclerViewAdapter
import com.freenow.assignment.abstraction.BaseViewHolder
import com.freenow.assignment.customViews.VehicleListItemView
import com.freenow.assignment.listener.VehiclesListCallbacks
import com.freenow.assignment.network.io.model.Vehicle
import com.freenow.assignment.ui_model.UIVehicleItem

class VehiclesListAdapter(
    private val listener: VehiclesListCallbacks?,
    context: Context,
    data: List<Vehicle>?
) : BaseRecyclerViewAdapter(context, data) {

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.populate(getData(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = VehicleListItemView(context).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        return SelectableVehicleViewHolder(listener, view, this)
    }

    class SelectableVehicleViewHolder(
        var listener: VehiclesListCallbacks?,
        private val vehicleITemView: VehicleListItemView,
        val adapter: VehiclesListAdapter
    ) :
        BaseViewHolder(vehicleITemView.rootView) {
        companion object {
            var lastSelectedVehicleIndex = -1
        }

        override fun populate(any: Any, position: Int) {
            val uiVehicle = any as UIVehicleItem
            vehicleITemView.updateUi(uiVehicle)
            // showing the selected/tapped item with different color
            vehicleITemView.setListener(object : VehicleListItemView.SelectableVehicleCallbacks {
                override fun onVehicleItemClicked(position: Int, vehicle: Vehicle) {
                    adapter.dataList?.let {
                        if (lastSelectedVehicleIndex == position) {
                            selectVehicle(position, false)
                            lastSelectedVehicleIndex = -1
                            listener?.onNothingSelected()
                        } else {
                            if (lastSelectedVehicleIndex != -1) {
                                selectVehicle(lastSelectedVehicleIndex, false)
                            }
                            selectVehicle(adapterPosition, true)
                            lastSelectedVehicleIndex = adapterPosition
                            adapter.notifyItemChanged(adapterPosition)
                            listener?.onVehicleSelected(adapterPosition, vehicle)
                        }
                    }
                }
            })
        }

        private fun selectVehicle(position: Int, selected: Boolean) {
            adapter.dataList?.let {
                (it[position] as UIVehicleItem).selected = selected
                adapter.notifyItemChanged(position)
            }
        }
    }
}
