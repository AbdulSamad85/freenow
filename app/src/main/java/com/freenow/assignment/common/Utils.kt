package com.freenow.assignment.common

import com.freenow.assignment.R
import com.freenow.assignment.enums.FleetType
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlin.math.abs

object Utils {
    fun getIconResourceId(fleetType: FleetType): Int {
        return when (fleetType) {
            FleetType.TAXI -> R.drawable.ic_taxi
            else -> R.drawable.ic_pool
        }
    }

    /**
     * Calculates the maximum zoom level to show all markers (copied code)
     */
    fun getMaxZoomedBounds(originalBounds: LatLngBounds): LatLngBounds {
        var bounds = originalBounds
        var sw = bounds.southwest
        var ne = bounds.northeast
        val deltaLat = abs(sw.latitude - ne.latitude)
        val deltaLon = abs(sw.longitude - ne.longitude)
        val zoomN = 0.005 // minimum zoom coefficient
        if (deltaLat < zoomN) {
            sw = LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude)
            ne = LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude)
            bounds = LatLngBounds(sw, ne)
        } else if (deltaLon < zoomN) {
            sw = LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2))
            ne = LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2))
            bounds = LatLngBounds(sw, ne)
        }
        return bounds
    }
}
