package com.freenow.assignment.network

import com.freenow.assignment.network.io.model.VehiclesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface VehicleService {
    @GET(".")
    suspend fun getVehicles(
        @Query("p1Lat") fromLatitude: Double,
        @Query("p1Lon") fromLongitude: Double,
        @Query("p2Lat") toLatitude: Double,
        @Query("p2Lon") toLongitude: Double
    ): Response<VehiclesResponse>
}
